## Covid19Bot

* Tuto Yoandev -> https://www.youtube.com/watch?v=6WE0Y0dwA8Y&ab_channel=YoanDev

Application qui récupère des données concernant le Covid19 en Json sur le site data.gouv
Puis crée un post sur twitter  (pour automatiser -> mettre en place un cron)
On ne peut pas poster 2 tweet identique successivement! 

# lancer script
``bash
php bin/console bot:post
``

# Mettre en place le cron
Voir cron guru 

cd /home/user/projet/covid19Bot && php bin/console post:bot   
