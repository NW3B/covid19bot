<?php

namespace App\Services;

use DateTime;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetDataService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;        
    }

    public function fromGouv(): string
    {
        $response = $this->client->request(
            'GET',
            'https://www.data.gouv.fr/fr/datasets/r/d2671c6c-c0eb-4e12-b69a-8e8f87fc224c'
        );

        $lastData = array_key_last($response->toArray());

        $date = new DateTime($response->toArray()[$lastData]['date']);
        //$reaTotale = $response->toArray()[$lastData]['reanimation'];
        $reaNouvelle = $response->toArray()[$lastData]['nouvellesReanimations'];
        //$gueris = $response->toArray()[$lastData]['gueris'];

        return 'Au ' . $date->format('d/m/Y') . ', il y a ' . $reaNouvelle . ' nouvelles personnes en réanimation.'; 
       // . 'Il y a ' . $reaTotale . 'personnes en réanimation.';
    }
}