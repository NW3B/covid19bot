<?php

namespace App\Services;

use Abraham\TwitterOAuth\TwitterOAuth;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class TwitterApiService
{
    private $getParams;

    public function __construct(ParameterBagInterface $getParams)
    {
        $this->getParams = $getParams;
    }

    public function post(string $content)
    {
        $consumerKey = $this->getParams->get('TWITTER_CONSUMER_KEY');
        $consumerSecret = $this->getParams->get('TWITTER_CONSUMER_SECRET');
        $accessToken = $this->getParams->get('TWITTER_ACCESS_TOKEN');
        $accessTokenSecret = $this->getParams->get('TWITTER_ACCESS_TOKEN_SECRET');
        $connection = new TwitterOAuth($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
        $connection->setApiVersion('2');

        //--- GET ---
        //$response = $connection->get('users', ['ids' => 12]);
       
        //--- POST ---
        //Pour que cette ligne fonctionne : changer $json false -> true dans methode post
        $response = $connection->post("tweets", ['text' => $content]);
        print_r($response);

    }
}